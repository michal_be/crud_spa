﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CRUD_Angular.Entities;
using CRUD_Angular.Models;

namespace CRUD_Angular.Repositiories
{
    public class ProductRepository: IProductRepository
    {

        private readonly ApplicationDbContext _context;
        public ProductRepository(ApplicationDbContext context)
        {
            _context = context;

        }

        public List<Product> GetAllProducts()
        {
            var productsList = _context.Products.ToList();
            return productsList;
        }

        public Product GetProductById(int productId)
        {
            var product = _context.Products.SingleOrDefault(x => x.ProductId == productId);
            return product;
        }

        public void AddProduct(Product product)
        {
            _context.Products.Add(product);
            _context.SaveChanges();
        }

        public void DeleteProduct(int productId)
        {
            var productToRemove = _context.Products.Single(x => x.ProductId == productId);

            _context.Products.Remove(productToRemove);
            _context.SaveChanges();
        }

        public void UpdateProduct(Product product)
        {
            var original = _context.Products.Find(product.ProductId);

            if (original != null)
            {
                _context.Entry(original).CurrentValues.SetValues(product);
                _context.SaveChanges();
            }
        }

    }
}