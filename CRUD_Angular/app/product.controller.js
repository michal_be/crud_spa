﻿(function () {
    'use strict';

    angular.module('app', ['ngRoute'])
        .config(function($routeProvider, $locationProvider) {
            $routeProvider
                .when("/edit/:productId", {
                    templateUrl: "Templates/edit.html",
                    controller: "editProductController",
                    controllerAs: "edit"
                })
                .when("/add", {
                    templateUrl: "Templates/add.html",
                    controller: "addProductController",
                    controllerAs: "add"
                })
                .when("/products", {
                    templateUrl: "Templates/products.html",
                    controller: "productController",
                    controllerAs: "vm"
                })
                .otherwise({
                    redirectTo: "/products"
                });
            $locationProvider.html5Mode(true);
        })
        .controller('productController', function($http) {

            var vm = this;
            var dataService = $http;
            vm.resetSearch = resetSearch;

            vm.showTable = true;
            vm.products = [];
            vm.errorText = " ";
            vm.searchInput = {
                Name: ''
            };

            function resetSearch() {
                Name: ''
            }
            

            dataService.get("api/product/getAll").then(function(result) {
                vm.products = result.data;
            }, function(error) {
                vm.errorText = "Cos poszlo nie tak";
            });

            vm.deleteProduct = function(product) {
                $http({ method: "POST", url: "api/product/deleteProduct/" + product.ProductId, data: product.ProductId, cache: false });
                $window.location.href = "/products";
            }

        })
        .controller('editProductController', function($http, $routeParams, $window) {

            var edit = this;
            var dataService = $http;
            edit.product = [];

            edit.uiState = {
                isValid: true,
                messages: []
            };

            dataService.get("api/product/getProduct/" + $routeParams.productId).then(function(result) {
                edit.product = result.data;
            }, function(error) {
                edit.errorText = "Cos poszlo nie tak";
            });


            edit.updateProduct = function (editProductForm) {
                if (editProductForm.$valid) {
                    if (validateData()) {
                        $http({ method: "POST", url: "api/product/updateProduct", data: edit.product, cache: false });
                        $window.location.href = "/products";
                    }
                }
                else {
                    editProductForm.$valid = false;
                    edit.uiState.isValid = false;
                }
            }
          
            function validateData() {
                edit.uiState.messages = [];
                if (edit.product.CreatedDate != null) {
                    if (isNaN(Date.parse(
                        edit.product.CreatedDate))) {
                        addValidationMessage('CreatedDate', 'Wprowadzono niepoprawną datę');
                    }
                }
                edit.uiState.isValid = (edit.uiState.messages.length == 0);

                return edit.uiState.isValid;
            }

            function addValidationMessage(prop, msg) {
                edit.uiState.messages.push({
                    property: prop,
                    message: msg
                });
            }

            edit.backToProductList = function() {
                $window.location.href = "/products";
            }

        })
        .controller('addProductController', function($http, $routeParams, $window) {

            var add = this;

            add.product = {
                ProductId: 0,
                Name: '',
                Description: '',
                Price: '',
                CreatedDate: ''
            };

            add.uiState = {
                isValid: true,
                messages: []
            };

            add.addProduct = function (addProductForm) {
                if (addProductForm.$valid) {
                    if (validateData()) {
                        $http({ method: "POST", url: "api/product/addProduct", data: add.product, cache: false });
                        $window.location.href = "/products";
                    }
                }
                else {
                    addProductForm.$valid = false;
                    add.uiState.isValid = false;
                }
            }

            function validateData() {          
                add.uiState.messages = [];
                if (add.product.CreatedDate != null) {
                    if (isNaN(Date.parse(
                        add.product.CreatedDate))) {
                        addValidationMessage('CreatedDate', 'Wprowadzono niepoprawną datę');
                    }
                }
                add.uiState.isValid = (add.uiState.messages.length == 0);
                
                return add.uiState.isValid;        
            }

            function addValidationMessage(prop, msg) {
                add.uiState.messages.push({
                    property: prop,
                    message: msg
                });
            }

            add.backToProductList = function() {
                $window.location.href = "/products";
            }

        });
    //.directive('formattedDate', function(dateFilter) {
    //    return {
    //        require: 'ngModel',
    //        scope: {
    //            format: "="
    //        },
    //        link: function(scope, element, attrs, ngModelController) {
    //            ngModelController.$parsers.push(function(data) {
    //                //convert data from view format to model format
    //                return dateFilter(data, scope.format); //converted
    //            });

    //            ngModelController.$formatters.push(function(data) {
    //                //convert data from model format to view format
    //                return dateFilter(data, scope.format); //converted
    //            });
    //        }
    //    }
    //});

})();