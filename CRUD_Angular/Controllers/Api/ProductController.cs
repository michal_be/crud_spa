﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CRUD_Angular.Entities;
using CRUD_Angular.Repositiories;

namespace CRUD_Angular.Controllers.Api
{
    public class ProductController : ApiController
    {
        private readonly IProductRepository _productRepository;

        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpGet]
        [Route("api/product/getAll")]
        public IHttpActionResult GetAll()
        {
            var products = _productRepository.GetAllProducts();
            return Ok(products);
        }

        [HttpGet]
        [Route("api/product/getProduct/{productId}")]
        public IHttpActionResult GetProductById(int productId)
        {
            var product = _productRepository.GetProductById(productId);
            return Ok(product);
        }

        [HttpPost]
        [Route("api/product/addProduct")]
        public IHttpActionResult AddProduct(Product product)
        {
            _productRepository.AddProduct(product);
            return Ok();
        }

        [HttpPost]
        [Route("api/product/deleteProduct/{productId}")]
        public IHttpActionResult DeleteProduct(int productId)
        {
            _productRepository.DeleteProduct(productId);
            return Ok();
        }

        [HttpPost]
        [Route("api/product/updateProduct")]
        public IHttpActionResult UpdateProduct(Product product)
        {
            _productRepository.UpdateProduct(product);
            return Ok();
        }
    }
}
