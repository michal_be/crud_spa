using CRUD_Angular.Entities;

namespace CRUD_Angular.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CRUD_Angular.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CRUD_Angular.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            if (!context.Products.Any())
            {
                context.Products.AddOrUpdate(
                  new Product { Name = "Telewizor", Description = "Marki Sony Bravia", Price = 1099, CreatedDate = DateTime.Today },
                  new Product { Name = "Krzes�o", Description = "Drewniane, prosto z IKEA", Price = 299, CreatedDate = new DateTime(2012, 11, 12) },
                  new Product { Name = "Basen ogrodowy", Description = "Dmuchany, o �rednicy 3m", Price = 999, CreatedDate = new DateTime(2014, 01, 02) }

                );
                context.SaveChanges();
            }

        }
    }
}
